# Personal MUTT conf
Personal `mutt` email client configuration

## Useful resources

- [Mutt folders wiki][1]
- [mutt modes: index, pager etc.][2]

[1]: https://gitlab.com/muttmua/mutt/wikis/MuttGuide/Folders
[2]: https://gitlab.com/muttmua/mutt/wikis/MuttGuide/Actions
