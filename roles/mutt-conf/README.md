mutt conf
=========

Personal `mutt` configuration

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Not required

License
-------

GPLv3

Author Information
------------------

Daniel Andrei Minca <dminca@protonmail.com>
